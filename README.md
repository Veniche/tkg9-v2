TK1 G9

Nama Anggota:
    Brian Christian
    Jessie Chandra
    Muhammad Naufal Nabiel
    Muhammad Raihan Andriqa
    Yoseline Chandra

Link herokuapp:
https://coronfront.herokuapp.com/

Kami membuat sebuah aplikasi yang dapat digunakan sebagai sarana untuk menyampaikan informasi tentang COVID-19 kepada masyarakat agar pengetahuan masyarakat Indonesia terhadap virus ini menjadi lebih luas dan tindakan pencegahan penyebaran virus COVID-19 di Indonesia dapat berjalan dengan lancar dan baik. Sebagian besar masyarakat masih kurang memahami dan
memandang pandemi ini dengan kurang serius, akibatnya laju penyebaran virus COVID-19 bertambah dengan cepat setiap harinya. Hal ini tentu saja merupakan suatu hal yang sangat buruk dan berbahaya bagi seluruh masyarakat Indonesia. Harapannya setelah mengunjungi aplikasi yang telah kami buat, mereka dapat menjadi sadar dan lebih peduli untuk membantu menekan
penyebaran COVID-19 di Indonesia dan menjadikan Indonesia sebagai negara yang lebih baik. Kami menamakan aplikasi kami Coronfront, yang berasal dari gabungan kata "corona" dan "front"
artinya agar aplikasi ini dapat menjadi yang terdepan dalam melawan pandemi virus COVID-19.

Fitur-fitur yang akan diimplementasikan dalam aplikasi kami, antara lain:
1.  Lapor COVID-19, fitur yang dapat digunakan untuk melaporkan diri jika tertular virus dan tindakan yang dapat dilakukan.
2.  Berita terbaru COVID-19, fitur untuk menampilkan berita-berita COVID-19 agar tidak ketinggalan informasi.
3.  Daftar rumah sakit yang melayani pasien COVID-19, agar pasien tidak perlu menghabiskan waktu untuk mencari rumah sakit yang dapat menangani virus.
4.  Data COVID-19 di Indonesia, sama seperti berita, namun lebih spesifik dalam penyebaran virus COVID di Indonesia.
5.  Edukasi dalam mencegah penyebaran COVID-19, agar masyarakat dapat lebih memahami dan dapat melakukan setiap langkah yang diperlukan untuk mencegah penyebaran virus.
6.  Pengetahuan umum tentang virus COVID-19, untuk menambahkan wawasan masyarakat terkait virus COVID-19 agar tindak pencegahan dapat berjalan lebih baik.
7.  About us, berisi asal usul pembuatan aplikasi ini.
8.  Contact, jika ada pertanyaan lebih lanjut dapat menghubungi kontak kami yang disediakan.

[![pipeline](https://gitlab.com/Veniche/project_tk_g9/badges/master/pipeline.svg)](http://coronfront.herokuapp.com)