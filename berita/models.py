from django.db import models
from django.urls import reverse

# Create your models here.
class Berita(models.Model):
    title = models.CharField(max_length=500, blank=True)
    description = models.TextField(max_length=5000, blank=True)
    document = models.ImageField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('berita:list')