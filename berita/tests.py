from django.test import TestCase, Client
from .models import Berita

# Create your tests here.
class Test(TestCase):
    def test_model_Berita(self):
        b = Berita.objects.create(
            title="Title",
            description="Aaabbbccc",
            document="image.jpg",
            uploaded_at="Nov 1st 2020")
        h = Berita.objects.all().count()
        self.assertEqual(h, 1)
        self.assertEquals('/berita/', b.get_absolute_url())
        
    def test_berita_view(self):
        response = self.client.get('/berita/')
        self.assertEqual(response.status_code,200)

    def test_berita_template_used(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'list_berita.html')
