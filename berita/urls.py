from django.urls import path
from . import views
from .views import AddBeritaView, BeritaView, BeritaDetailView
from django.views.decorators.csrf import csrf_exempt

app_name = 'berita'

urlpatterns = [
    path('upload', AddBeritaView.as_view(), name='upload'),
    path('', BeritaView.as_view(), name='list'),
    path('detail/<int:pk>', BeritaDetailView.as_view(), name='detail'),
    path("cari", csrf_exempt(views.cari), name="cari"),
]