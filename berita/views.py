from django.shortcuts import redirect, render
from django.views.generic import ListView, DetailView, CreateView
from .models import Berita
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core import serializers
import json
from django.http import HttpResponse
from django.http import JsonResponse

# Create your views here.
@method_decorator(login_required(login_url='login'), name='dispatch')
class AddBeritaView(CreateView):
    model = Berita
    template_name = 'upload_berita.html'
    fields = '__all__'

class BeritaView(ListView):
    model = Berita
    template_name = 'list_berita.html'
    context_object_name = 'berita'
    paginate_by = 4
    queryset = Berita.objects.all()

class BeritaDetailView(DetailView):
    model = Berita
    template_name = 'detail_berita.html'

def cari(request):
    if request.method == "POST":
        search_str = json.loads(request.body).get('searchText')
        beritas = Berita.objects.filter(title__icontains=search_str) | Berita.objects.filter(uploaded_at__istartswith=search_str)
        data = beritas.values()
        return JsonResponse(list(data), safe=False)