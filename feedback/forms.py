from django.forms import ModelForm
from .models import feedbackModel
from django import forms

class feedbackForm(forms.ModelForm) :
    class Meta:
        model = feedbackModel
        fields = [
            'Nama',
            'Email',
            'Kritik',
        ]
        widgets={
            'Nama' : forms.TextInput(attrs={'class' : 'input', 'id' : 'Nama','placeholder' : 'Ani Budi','required' : True}),
            'Email': forms.EmailInput(attrs={'class': 'input', 'id' : 'Email', 'placeholder' : 'Anibudi@gmail.com'}),
            'Kritik' : forms.Textarea(attrs={'class' : 'textarea', 'id' : 'Kritik', 'rows' : 5}),
        }
