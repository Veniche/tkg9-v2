from django.db import models

# Create your models here.

class feedbackModel(models.Model):
    Nama = models.CharField(max_length=120)
    Email = models.EmailField(max_length=50)
    Kritik = models.TextField(blank=False, null=False)