$(document).ready(function() {
    $("#table_button").click(function() {
        $("#table_feedback").slideToggle(1000, function() {
            if($("#table_button").val() == "close")
            {
                $("#table_button").val("Show Feedback Response");
            }
            else
            {
                $("#table_button").val("close");
            }
        });
    });

    $("textarea").focus(function(){
        $(this).css("background-color", "#ff6969");
    });
    $("textarea").blur(function(){
        $(this).css("background-color", "white");
    });

    if (validateUsername($('#Nama').val())) {
        $.ajax({
            type: "POST",
            url: "feedback",
            data: {
                Nama: $('#Nama').val(),
                Email: $('#Email').val(),
                Kritik: $('#Kritik').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            },
        // success: function(response) {
        //     popup(response.message);

        // }
        })
    }

});