from django.test import TestCase, Client
from .models import feedbackModel
from .forms import feedbackForm

# Create your tests here.

class Test(TestCase):
    def test_feedback_view(self):
        response = self.client.get('/feedback')
        self.assertEqual(response.status_code,200)

    def test_feedback_model(self):
        feedback = feedbackModel.objects.create(Nama="ani", Email="ani@gmail.com" , Kritik="bagus")
        count = feedbackModel.objects.all().count()
        self.assertEqual(count,1)

    def test_feedback_template_used(self):
        response = Client().get('/feedback')
        self.assertTemplateUsed(response, 'feedback_form.html')

    def test_post_feedback(self):
       response = Client().post('/feedback',{'Nama':'jon'})
       self.assertIn('jon', response.content.decode())

    def test_form_is_valid(self) :
        feedback_form = feedbackForm(data={'Nama':"ani", 'Email':"ani@gmail.com" , 'Kritik':"bagus"})
        self.assertTrue(feedback_form.is_valid())

    def test_send_feedback(self):
        response = Client().get('/feedback')
        contents = response.content.decode('utf8')
        self.assertIn("Nama", contents)
        self.assertIn("Email", contents)
        self.assertIn("Kritik", contents)





