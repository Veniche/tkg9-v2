from django.urls import path
from django.conf.urls import url
from . import views
from django.views.decorators.csrf import csrf_exempt

app_name = 'modelraihan'

urlpatterns = [
    path('', views.formku, name='form'),
    path('register', views.register, name="register"),
    path('login', views.loginCustom, name='login'),
    path('logout', views.logoutCustom, name='logout'),
    path('ajax/validate_username/',
         views.validate_username, name='validate_username'),
    path("cariGejala", csrf_exempt(views.cariGejala), name="cariGejala"),
]
