from django.db import models
from django.urls import reverse

# Create your models here.


class tanyakan(models.Model):
    namaPenanya = models.CharField(
        max_length=30, default="Nama Penanya", blank=False)
    emailPenanya = models.CharField(
        max_length=30, default="Covid@covidmail.com", blank=False)
    pertanyaan = models.TextField(
        max_length=300, default="Tuliskan apa yang ingin kamu ketahui", blank=False)


def get_absolute_url(self):
    return reverse('home:tanya')
