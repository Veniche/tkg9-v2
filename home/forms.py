from django.forms import ModelForm
from .models import tanyakan
from django import forms


class tanyaForm(forms.ModelForm):
    class Meta:
        model = tanyakan
        fields = [
            'namaPenanya',
            'emailPenanya',
            'pertanyaan',

        ]
        widgets = {
            'Nama': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Oka Septiatri', 'required': True}),
            'Email': forms.EmailInput(attrs={'class': 'input', 'placeholder': 'SeptiOka@gmail.com'}),
            'Pertanyaan': forms.Textarea(attrs={'class': 'textarea', 'rows': 5}),

        }
